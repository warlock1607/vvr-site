// const nodeExternals = require('webpack-node-externals');
const LoadablePlugin = require('@loadable/webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { InjectManifest } = require('workbox-webpack-plugin');

const mode = process.env.NODE_ENV;

const config = {
  mode,
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
};

const clientConfig = {
  ...config,
  entry: {
    main: './src/client/index.js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: '> 0.25%, not dead',
                    useBuiltIns: 'usage',
                    corejs: { version: 3, proposals: true },
                  },
                ],
                '@babel/preset-react',
              ],
              plugins: ['@loadable/babel-plugin'],
            },
          },
          'eslint-loader',
        ],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ['**/*.js'],
    }),
    new LoadablePlugin(),
    new InjectManifest({
      swSrc: './src/client/sw.js',
    }),
  ],
  output: {
    path: `${__dirname}/dist`,
    filename: '[name].js',
    publicPath: '/',
  },
};

const serverConfig = {
  ...config,
  entry: {
    main: './src/server/index.js',
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: 'node 10',
                  },
                ],
                '@babel/preset-react',
              ],
              plugins: ['@loadable/babel-plugin'],
            },
          },
          'eslint-loader',
        ],
      },
    ],
  },
  plugins: [new LoadablePlugin()],
  // externals: nodeExternals(),
  output: {
    path: `${__dirname}/server`,
    filename: '[name].js',
    publicPath: '/',
  },
};

module.exports = [clientConfig, serverConfig];
