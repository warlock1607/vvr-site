const htmlTemplate = (html, css, scripts) =>
  `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="React SPA template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='theme-color' content='#333333'>
    <link rel='manifest' href='/manifest.json'>
    <link rel='icon' type='image/png' href='/favicon.png'>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
    <title>React SSR template</title>
    <style id="jss-server-side">${css}</style>
</head>   
<body style="margin: 0;">
    <div id="root">${html}</div>
    <script>
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', function() {
            navigator.serviceWorker.register('/sw.js');
        });
    }
    </script>
    ${scripts}
</body>
</html>`;

export default htmlTemplate;
