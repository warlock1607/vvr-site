import express from 'express';
import cors from 'cors';

import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter, matchPath } from 'react-router-dom';
import { ChunkExtractor } from '@loadable/server';
import { ServerStyleSheets, ThemeProvider } from '@material-ui/styles';

import htmlTemplate from './htmlTemplate';
import App from '../shared/App';
import routes from '../shared/routes';
import theme from '../shared/theme';
import config from '../../config';

const app = express();

app.use(cors());
app.use(express.static('dist'));

const stats = '../dist/loadable-stats.json';

app.get('*', (req, res) => {
  const context = routes.find(route => matchPath(req.url, route)) || {};
  const extractor = new ChunkExtractor({ statsFile: stats });
  const sheets = new ServerStyleSheets();

  const components = extractor.collectChunks(
    <ThemeProvider theme={theme}>
      <StaticRouter location={req.url} context={context}>
        <App />
      </StaticRouter>
    </ThemeProvider>
  );

  const html = renderToString(sheets.collect(components));
  const scripts = extractor.getScriptTags();
  const css = sheets.toString();

  res.send(htmlTemplate(html, css, scripts));
});

app.listen(config.port);
