/* eslint-disable */

workbox.setConfig({
  debug: false,
});

workbox.core.skipWaiting();

workbox.core.clientsClaim();

self.__precacheManifest = ['/', '/about'].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerRoute(
  /\.(?:png|jpg|jpeg|svg|gif)$/,
  new workbox.strategies.CacheFirst({
    cacheName: 'image-cache',
  })
);

workbox.routing.registerRoute(
  '/manifest.json',
  new workbox.strategies.CacheFirst({
    cacheName: 'manifest-cache',
  })
);
