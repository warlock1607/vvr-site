import 'core-js/modules/es.array.includes'; // force include polyfill for loadable script
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { loadableReady } from '@loadable/component';
import { ThemeProvider } from '@material-ui/styles';

import App from '../shared/App';
import theme from '../shared/theme';

function Main() {
  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ThemeProvider>
  );
}

loadableReady(() => {
  ReactDOM.hydrate(<Main />, document.getElementById('root'));
});
