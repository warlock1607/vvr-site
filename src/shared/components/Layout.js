import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';

import RouteList from './RouteList';

const useStyles = makeStyles(theme => ({
  withDrawer: {
    marginLeft: theme.drawerWidth,
    width: `calc(100% - ${theme.drawerWidth}px)`,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  sideMenu: {
    width: theme.drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  navPaper: {
    width: theme.drawerWidth,
  },
  navHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const Layout = ({ children, minimal }) => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  return (
    <>
      <AppBar
        className={minimal ? undefined : classes.withDrawer}
        position="fixed"
      >
        <Toolbar>
          {minimal && (
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
              onClick={() => setOpen(!open)}
            >
              <MenuIcon />
            </IconButton>
          )}
        </Toolbar>
      </AppBar>
      <Drawer
        variant={minimal ? 'temporary' : 'permanent'}
        open={open}
        onClose={minimal ? () => setOpen(false) : undefined}
        PaperProps={{ classes: { root: classes.navPaper } }}
      >
        <div className={`${classes.toolbar} ${classes.navHeader}`}>
          <p>Menu</p>
        </div>
        <Divider />
        <RouteList handleClose={minimal ? () => setOpen(false) : undefined} />
      </Drawer>
      <main className={minimal ? undefined : classes.withDrawer}>
        {children}
      </main>
    </>
  );
};

Layout.defaultProps = {
  minimal: false,
};

Layout.propTypes = {
  children: PropTypes.element.isRequired,
  minimal: PropTypes.bool,
};

export default Layout;
