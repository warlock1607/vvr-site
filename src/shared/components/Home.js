import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Hidden } from '@material-ui/core';

import HomeCard from './HomeCard';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  toolbar: theme.mixins.toolbar,
  container: {
    padding: theme.spacing(2),
    display: 'flex',
  },
  content: {
    marginTop: '10vh',
  },
  [theme.breakpoints.down('sm')]: {
    content: {
      width: '100%',
    },
  },
  [theme.breakpoints.up('md')]: {
    content: {
      width: '50%',
    },
  },
  imgBox: {
    width: '50%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    width: '80%',
    height: 'auto',
  },
}));

const Home = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.toolbar} />
      <div className={classes.container}>
        <div className={classes.content}>
          <Typography color="primary" variant="h2">
            LAZARUS
          </Typography>
          <Typography variant="subtitle1">
            ITASU NITU MISIS Laboratory of Cybernetics and Software Development
          </Typography>
          <HomeCard />
        </div>
        <Hidden smDown>
          <div className={classes.imgBox}>
            <img className={classes.img} alt="img" src="/home.png" />
          </div>
        </Hidden>
      </div>
    </div>
  );
};

export default Home;
