import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import routes from '../routes';

const ListItemLink = props => <ListItem button component={Link} {...props} />;

const RouteList = ({ handleClose, location }) => {
  const renderListItem = (title, path) => (
    <ListItemLink
      selected={location.pathname === path}
      key={title.toLowerCase()}
      onClick={handleClose}
      to={path}
    >
      <ListItemText primary={title} />
    </ListItemLink>
  );

  return (
    <List component="nav">
      {routes
        .filter(route => route.static === true)
        .map(route => renderListItem(route.title, route.path))}
    </List>
  );
};

RouteList.defaultProps = {
  handleClose: undefined,
};

RouteList.propTypes = {
  location: PropTypes.shape({ pathname: PropTypes.string.isRequired })
    .isRequired,
  handleClose: PropTypes.func,
};

export default withRouter(RouteList);
