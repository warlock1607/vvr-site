import loadable from '@loadable/component';

const Home = loadable(() => import('./components/Home'));
const About = loadable(() => import('./components/About'));

const routes = [
  {
    title: 'Home',
    static: true,
    path: '/',
    exact: true,
    component: Home,
  },
  {
    title: 'About us',
    static: true,
    path: '/about',
    exact: true,
    component: About,
  },
];

export default routes;
