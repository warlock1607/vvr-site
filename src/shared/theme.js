import { createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#7335d4',
    },
    secondary: {
      main: '#039881',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
  drawerWidth: 240,
});

export default theme;
